<?php
namespace Neos\Flow\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs! This block will be used as the migration description if getDescription() is not used.
 */
class Version20161130182243 extends AbstractMigration
{

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @param Schema $schema
     * @return void
     */
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on "mysql".');
        
        $this->addSql('ALTER TABLE one50_twint_domain_model_ordermapping DROP FOREIGN KEY FK_FEDDE099F5299398');
        $this->addSql('DROP INDEX UNIQ_FEDDE099F5299398 ON one50_twint_domain_model_ordermapping');
        $this->addSql('ALTER TABLE one50_twint_domain_model_ordermapping ADD twintorderuuid VARCHAR(36) NOT NULL, DROP twintorder, CHANGE `order` shoporder VARCHAR(40) DEFAULT NULL');
        $this->addSql('ALTER TABLE one50_twint_domain_model_ordermapping ADD CONSTRAINT FK_FEDDE09969FE05F8 FOREIGN KEY (shoporder) REFERENCES one50_shop_domain_model_order (persistence_object_identifier)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FEDDE09969FE05F8 ON one50_twint_domain_model_ordermapping (shoporder)');
    }

    /**
     * @param Schema $schema
     * @return void
     */
    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on "mysql".');
        
        $this->addSql('ALTER TABLE one50_twint_domain_model_ordermapping DROP FOREIGN KEY FK_FEDDE09969FE05F8');
        $this->addSql('DROP INDEX UNIQ_FEDDE09969FE05F8 ON one50_twint_domain_model_ordermapping');
        $this->addSql('ALTER TABLE one50_twint_domain_model_ordermapping ADD twintorder VARCHAR(36) DEFAULT \'\' NOT NULL COLLATE utf8_unicode_ci, DROP twintorderuuid, CHANGE shoporder `order` VARCHAR(40) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE one50_twint_domain_model_ordermapping ADD CONSTRAINT FK_FEDDE099F5299398 FOREIGN KEY (`order`) REFERENCES one50_shop_domain_model_order (persistence_object_identifier)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FEDDE099F5299398 ON one50_twint_domain_model_ordermapping (`order`)');
    }
}