<?php
namespace One50\Twint\Domain\Repository;

/*
 * This file is part of the One50.Twint package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class PairingRepository extends Repository
{

    // add customized methods here

}
