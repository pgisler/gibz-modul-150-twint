<?php
namespace One50\Twint\Domain\Model;

/*
 * This file is part of the One50.Twint package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class TwintCustomerRelation {
	
	/**
	 * @var \One50\Shop\Domain\Model\User
	 * @ORM\OneToOne
	 */
	protected $customer;
	
	/**
	 * @var string
	 * @ORM\Column(length=36)
	 */
	protected $twintCustomerRelation;
	
	
	/**
	 * @return \One50\Shop\Domain\Model\User
	 */
	public function getCustomer() {
		return $this->customer;
	}
	
	/**
	 * @param \One50\Shop\Domain\Model\User $customer
	 * @return void
	 */
	public function setCustomer(\One50\Shop\Domain\Model\User $customer) {
		$this->customer = $customer;
	}
	
	/**
	 * @return string
	 */
	public function getTwintCustomerRelation() {
		return $this->twintCustomerRelation;
	}
	
	/**
	 * @param string $twintCustomerRelation
	 * @return void
	 */
	public function setTwintCustomerRelation($twintCustomerRelation) {
		$this->twintCustomerRelation = $twintCustomerRelation;
	}
	
}
