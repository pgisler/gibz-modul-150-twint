<?php
namespace One50\Twint\Domain\Model;

/*
 * This file is part of the One50.Twint package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Pairing {
	
	/**
	 * @var \One50\Shop\Domain\Model\User
	 * @ORM\ManyToOne
	 */
	protected $user;
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @ORM\Column(length=36)
	 */
	protected $pairingUuid;
	
	/**
	 * @var string
	 * @ORM\Column(length=36, nullable=true))
	 */
	protected $twintOrderUuid;
	
	
	/**
	 * @return \One50\Shop\Domain\Model\User
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * @param \One50\Shop\Domain\Model\User $user
	 * @return void
	 */
	public function setUser(\One50\Shop\Domain\Model\User $user) {
		$this->user = $user;
	}
	
	/**
	 * @return string
	 */
	public function getPairingUuid() {
		return $this->pairingUuid;
	}
	
	/**
	 * @param string $pairingUuid
	 * @return void
	 */
	public function setPairingUuid($pairingUuid) {
		$this->pairingUuid = $pairingUuid;
	}
	
	/**
	 * @return string
	 */
	public function getTwintOrderUuid() {
		return $this->twintOrderUuid;
	}
	
	/**
	 * @param string $twintOrderUuid
	 */
	public function setTwintOrderUuid($twintOrderUuid) {
		$this->twintOrderUuid = $twintOrderUuid;
	}
	
}
