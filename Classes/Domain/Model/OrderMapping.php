<?php
namespace One50\Twint\Domain\Model;

/*
 * This file is part of the One50.Twint package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class OrderMapping {
	
	/**
	 * @var \One50\Shop\Domain\Model\Order
	 * @ORM\OneToOne
	 */
	protected $shopOrder;
	
	/**
	 * @var string
	 * @Flow\Validate(type="NotEmpty")
	 * @ORM\Column(length=36)
	 */
	protected $twintOrderUuid;
	
	
	/**
	 * @return \One50\Shop\Domain\Model\Order
	 */
	public function getShopOrder() {
		return $this->shopOrder;
	}
	
	/**
	 * @param \One50\Shop\Domain\Model\Order $shopOrder
	 * @return void
	 */
	public function setShopOrder(\One50\Shop\Domain\Model\Order $shopOrder) {
		$this->shopOrder = $shopOrder;
	}
	
	/**
	 * @return string
	 */
	public function getTwintOrderUuid() {
		return $this->twintOrderUuid;
	}
	
	/**
	 * @param string $twintOrderUuid
	 * @return void
	 */
	public function setTwintOrderUuid($twintOrderUuid) {
		$this->twintOrderUuid = $twintOrderUuid;
	}
	
}
