<?php
namespace One50\Twint\Command;

/*
 * This file is part of the One50.Twint package.
 */

use One50\Shop\Domain\Model\User;
use One50\Shop\Domain\Repository\UserRepository;
use One50\Twint\Service\SoapService;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\PersistenceManagerInterface;
use Neos\Flow\ResourceManagement\ResourceManager;

/**
 * @Flow\Scope("singleton")
 */
class TwintCommandController extends \Neos\Flow\Cli\CommandController {
	
	/**
	 * @var ResourceManager
	 * @Flow\Inject
	 */
	protected $resourceManager;
	
	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 */
	protected $persistenceManager;
	
	/**
	 * @var UserRepository
	 * @Flow\Inject
	 */
	protected $userRepository;
	
	/**
	 * Soap Service
	 *
	 * @var SoapService
	 * @Flow\Inject
	 */
	protected $soapService;
	
	/**
	 * Contains the settings defined in Configuration/Settings.yaml
	 *
	 * @var array
	 * @Flow\InjectConfiguration(path="settings")
	 */
	protected $settings;
	
	/**
	 * Outputs a settings value to the console
	 *
	 * @param string $path
	 */
	public function settingsCommand($path) {
		$parts = explode('.', $path);
		$output = $this->settings;
		
		if (!empty($parts)) {
			foreach ($parts as $part) {
				if (array_key_exists($part, $output)) {
					$output = $output[$part];
				} else {
					$this->outputLine('invalid path');
					
					return;
				}
			}
		}
		
		$this->outputLine($output);
	}
	
	/**
	 * Outputs the json encodes response from the "checkSystemStatus" SOAP Call
	 */
	public function checkSystemStatusCommand() {
		$response = $this->soapService->checkSystemStatus();
		$this->outputLine($response);
	}
	
	/**
	 * Enroll cash register
	 */
	public function enrollCashRegisterCommand() {
		$response = $this->soapService->enrollCashRegister();
		$this->outputLine(json_encode($response));
	}
	
	/**
	 * Request checking for an user
	 *
	 * @param string $userUuid
	 */
	public function requestCheckInCommand($userUuid) {
		$user = $this->persistenceManager->getObjectByIdentifier($userUuid, 'One50\\Shop\\Domain\\Model\\User');
		if ($user instanceof User) {
			$this->outputLine("User: {$user->getFirstName()} {$user->getLastName()}");
			$response = $this->soapService->requestCheckIn($user);
			$this->outputLine(json_encode($response));
		} else {
			$this->outputLine('No user with identifier "' . $userUuid . '" found!"');
		}
	}
	
	/**
	 * Monitor a pairing with a given pairing uuid
	 *
	 * @param string $pairingUuid
	 */
	public function monitorCheckInCommand($pairingUuid) {
		$response = $this->soapService->monitorCheckIn($pairingUuid);
		$this->outputLine(json_encode($response));
	}
	
	/**
	 * Cancel a checkIn process with a given pairing uuid an reason
	 *
	 * @param string $pairingUuid
	 * @param string $reason
	 */
	public function cancelCheckInCommand($pairingUuid, $reason) {
		$reasons = array(
			1 => SoapService::CANCEL_CHECKIN_REASON_INVALID_PAIRING,
			2 => SoapService::CANCEL_CHECKIN_REASON_OTHER_PAYMENT_METHOD,
			3 => SoapService::CANCEL_CHECKIN_REASON_PAYMENT_ABORT,
			4 => SoapService::CANCEL_CHECKIN_REASON_NO_PAYMENT_NEEDED
		);
		
		if (!in_array($reason, $reasons)) {
			if (array_key_exists($reason, $reasons)) {
				$reason = $reasons[$reason];
			} else {
				$this->outputLine('Invalid reason: "' . $reason . '"');
				
				return;
			}
		}
		
		$response = $this->soapService->cancelCheckIn($pairingUuid, $reason);
		$this->outputLine(json_encode($response));
	}
	
	/**
	 * Start an order
	 *
	 * @param float  $amount
	 * @param string $transactionReference
	 * @param string $pairingUuid
	 * @param string $customerRelationUuid
	 */
	public function startOrderCommand($amount, $transactionReference, $pairingUuid, $customerRelationUuid) {
		if ($pairingUuid == 0) {
			$pairingUuid = null;
		}
		
		if ($customerRelationUuid == 0) {
			$customerRelationUuid = null;
		}
		
		$response = $this->soapService->startOrder($amount, $transactionReference, $pairingUuid, $customerRelationUuid);
		$this->outputLine(json_encode($response));
	}
	
	/**
	 * Monitor a specific order
	 *
	 * @param string $orderUuid
	 */
	public function monitorOrderCommand($orderUuid) {
		$response = $this->soapService->monitorOrderStatus($orderUuid);
		$this->outputLine(json_encode($response));
	}
	
	/**
	 * Cancel a specific order
	 *
	 * @param string $orderUuid
	 */
	public function cancelOrderCommand($orderUuid) {
		$response = $this->soapService->cancelOrder($orderUuid);
		$this->outputLine(json_encode($response));
	}
	
}
