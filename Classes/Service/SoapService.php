<?php
namespace One50\Twint\Service;

use One50\Shop\Domain\Model\User;
use One50\Twint\Domain\Model\Pairing;
use One50\Twint\Domain\Model\TwintCustomerRelation;
use One50\Twint\Domain\Repository\PairingRepository;
use One50\Twint\Domain\Repository\TwintCustomerRelationRepository;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Error\Exception;
use Neos\Flow\Persistence\PersistenceManagerInterface;
use Neos\Flow\ResourceManagement\ResourceManager;
use Neos\Utility\Files;

/**
 * Class SoapService
 *
 * @package One50\Twint\Service
 * @Flow\Scope("singleton")
 */
class SoapService {
	
	const OPERATION_RESULT_TYPE_OK = 'OK';
	
	const CASH_REGISTER_TYPE_EPOS = 'EPOS';
	
	const CANCEL_CHECKIN_REASON_INVALID_PAIRING = 'INVALID_PAIRING';
	const CANCEL_CHECKIN_REASON_OTHER_PAYMENT_METHOD = 'OTHER_PAYMENT_METHOD';
	const CANCEL_CHECKIN_REASON_PAYMENT_ABORT = 'PAYMENT_ABORT';
	const CANCEL_CHECKIN_REASON_NO_PAYMENT_NEEDED = 'NO_PAYMENT_NEEDED';
	
	const PAYMENT_METHOD_IMMEDIATE = 'PAYMENT_IMMEDIATE';
	const PAYMENT_METHOD_DEFERRED = 'PAYMENT_DEFERRED';
	
	const POSTING_TYPE_GOODS = 'GOODS';
	const POSTING_TYPE_MONEY = 'MONEY';
	
	const CURRENCY_CHF = 'CHF';
	
	const PAIRING_STATUS_NO_PAIRING = 'NO_PAIRING';
	const PAIRING_STATUS_IN_PROGRESS = 'PAIRING_IN_PROGRESS';
	const PAIRING_STATUS_ACTIVE = 'PAIRING_ACTIVE';
	
	const ORDER_STATUS_SUCCESS = 0;
	const ORDER_STATUS_IN_PROGRESS = 1;
	const ORDER_STATUS_FAILURE = 2;
	
	const ORDER_REASON_ORDER_OK = 0;
	const ORDER_REASON_ORDER_PARTIAL_OK = 1;
	const ORDER_REASON_ORDER_RECEIVED = 80;
	const ORDER_REASON_ORDER_PENDING = 90;
	const ORDER_REASON_ORDER_CONFIRMATION_PENDING = 91;
	const ORDER_REASON_GENERAL_ERROR = 100;
	const ORDER_REASON_CLIENT_TIMEOUT = 101;
	const ORDER_REASON_MERCHANT_ABORT = 102;
	const ORDER_REASON_CLIENT_ABORT = 103;
	
	/**
	 * Contains the settings defined in Configuration/Settings.yaml
	 *
	 * @var array
	 * @Flow\InjectConfiguration(path="settings")
	 */
	protected $settings;
	
	/**
	 * @var ResourceManager
	 * @Flow\Inject
	 */
	protected $resourceManager;
	
	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 */
	protected $persistenceManager;
	
	/**
	 * @var PairingRepository
	 * @Flow\Inject
	 */
	protected $pairingRepository;
	
	/**
	 * @var TwintCustomerRelationRepository
	 * @Flow\Inject
	 */
	protected $twintCustomerRelationRepository;
	
	/**
	 * Soap Client
	 *
	 * @var \SoapClient
	 */
	protected $soapClient;
	
	/**
	 * Merchant Information array containing merchant uuid / cash register id
	 *
	 * @var array
	 */
	protected $merchantInformation;
	
	
	/**
	 * Checks the system status
	 *
	 * @return mixed
	 */
	public function checkSystemStatus() {
		// get soap client
		$soapClient = $this->getSoapClient();
		
		// Prepare the parameters for the request
		$params = array("MerchantInformation" => $this->merchantInformation);
		
		/* Invoke webservice method with your parameters */
		$soapClient->__setSoapHeaders($this->getHeader());
		$response = null;
		
		try {
			$response = $soapClient->checkSystemStatus($params);
		} catch (\SoapFault $soapFault) {
			$this->exitWithError($soapFault);
		}
		
		return $response->Status;
	}
	
	/**
	 * Enrolls a cash register of type "EPOS"
	 *
	 * @return mixed
	 */
	public function enrollCashRegister() {
		// get soap client
		$soapClient = $this->getSoapClient();
		
		// Prepare the parameters for the request
		$params = array(
			"MerchantInformation" => $this->merchantInformation,
			"CashRegisterType"    => self::CASH_REGISTER_TYPE_EPOS
		);
		
		/* Invoke webservice method with your parameters */
		$soapClient->__setSoapHeaders($this->getHeader());
		$response = null;
		
		try {
			$response = $soapClient->enrollCashRegister($params);
		} catch (\SoapFault $soapFault) {
			$this->exitWithError($soapFault);
		}
		
		return $response;
	}
	
	/**
	 * Initiates the pairing with a customer
	 *
	 * @param User $user
	 * @param bool $qrCodeRendering
	 * @return mixed
	 */
	public function requestCheckIn(User $user, $qrCodeRendering = false) {
		// get soap client
		$soapClient = $this->getSoapClient();
		
		// Prepare the parameters for the request
		$params = array(
			"MerchantInformation" => $this->merchantInformation,
			"QRCodeRendering"     => $qrCodeRendering,
		);
		
		$this->addCustomerIdentification($params, $user);
		
		/* Invoke webservice method with your parameters */
		$soapClient->__setSoapHeaders($this->getHeader());
		$response = null;
		
		try {
			$response = $soapClient->requestCheckIn($params);
		} catch (\SoapFault $soapFault) {
			$this->exitWithError($soapFault);
		}
		
		$pairingUuid = $response->CheckInNotification->PairingUuid;
		$this->persistPairing($user, $pairingUuid);
		
		return $response;
	}
	
	/**
	 * Retrieve current status information about a specific pairing process
	 *
	 * @param $pairingUuid
	 * @return mixed
	 */
	public function monitorCheckIn($pairingUuid) {
		// get soap client
		$soapClient = $this->getSoapClient();
		
		// Prepare the parameters for the request
		$params = array(
			"MerchantInformation" => $this->merchantInformation,
			"PairingUuid"         => $pairingUuid
		);
		
		/* Invoke webservice method with your parameters */
		$soapClient->__setSoapHeaders($this->getHeader());
		$response = null;
		
		try {
			$response = $soapClient->monitorCheckIn($params);
		} catch (\SoapFault $soapFault) {
			$this->exitWithError($soapFault);
		}
		
		if ($response->CheckInNotification->PairingStatus == self::PAIRING_STATUS_ACTIVE) {
			// @TODO: ist customerRelationUuid available?
			if (isset($response->CheckInNotification->CustomerInformation->CustomerRelationUuid)) {
				$pairing = $this->pairingRepository->findOneByPairingUuid($pairingUuid);
				if ($pairing instanceof Pairing) {
					$customerRelationUuid = $response->CheckInNotification->CustomerInformation->CustomerRelationUuid;
					$this->updateCustomerRelation($pairing->getUser(), $customerRelationUuid);
				}
			}
		}
		
		return $response;
	}
	
	/**
	 * Cancel a specific checkIn process because of $reason
	 *
	 * @param $pairingUuid
	 * @param $reason
	 * @return mixed
	 */
	public function cancelCheckIn($pairingUuid, $reason) {
		// get soap client
		$soapClient = $this->getSoapClient();
		
		// Prepare the parameters for the request
		$params = array(
			"MerchantInformation" => $this->merchantInformation,
			"Reason"              => $reason,
			"PairingUuid"         => $pairingUuid
		);
		
		/* Invoke webservice method with your parameters */
		$soapClient->__setSoapHeaders($this->getHeader());
		$response = null;
		
		try {
			$response = $soapClient->cancelCheckIn($params);
		} catch (\SoapFault $soapFault) {
			$this->exitWithError($soapFault);
		}
		
		return $response;
	}
	
	/**
	 * Start an order
	 *
	 * @param float  $amount
	 * @param string $transactionReference
	 * @param string $pairingUuid
	 * @param string $customerRelationUuid
	 * @return mixed
	 * @throws Exception
	 */
	public function startOrder($amount, $transactionReference, $pairingUuid = null, $customerRelationUuid = null) {
		// get soap client
		$soapClient = $this->getSoapClient();
		
		// Prepare the parameters for the request
		$params = array(
			"MerchantInformation" => $this->merchantInformation,
			"Order"               => array(
				"type"                         => self::PAYMENT_METHOD_IMMEDIATE,
				"PostingType"                  => self::POSTING_TYPE_GOODS,
				"confirmationNeeded"           => false,
				"RequestedAmount"              => array(
					"Amount"   => $amount,
					"Currency" => self::CURRENCY_CHF
				),
				"MerchantTransactionReference" => $transactionReference,
			),
		);
		
		// check customer identification method
		if (is_null($pairingUuid) && is_null($customerRelationUuid)) {
			throw new Exception("Invalid customer identification in 'startOrder': Either pairingUuid or customerRelationUuid is required!");
		} elseif (!is_null($customerRelationUuid)) {
			$params['CustomerRelationUuid'] = $customerRelationUuid;
		} else {
			$params['PairingUuid'] = $pairingUuid;
		}
		
		// Invoke webservice method with your parameters
		$soapClient->__setSoapHeaders($this->getHeader());
		$response = null;
		
		try {
			$response = $soapClient->startOrder($params);
			
			if (isset($response->OrderUuid)) {
				if (!is_null($pairingUuid)) {
					/** @var Pairing $pairing */
					$pairing = $this->pairingRepository->findOneByPairingUuid($pairingUuid);
					if ($pairing instanceof Pairing) {
						$pairing->setTwintOrderUuid($response->OrderUuid);
						$this->pairingRepository->update($pairing);
						$this->persistenceManager->persistAll();
					}
				}
			}
		} catch (\SoapFault $soapFault) {
			$this->exitWithError($soapFault);
		}
		
		return $response;
	}
	
	/**
	 * Monitor a specific order
	 *
	 * @param string $orderUuid
	 * @return mixed
	 */
	public function monitorOrderStatus($orderUuid) {
		// get soap client
		$soapClient = $this->getSoapClient();
		
		// Prepare the parameters for the request
		$params = array(
			"MerchantInformation" => $this->merchantInformation,
			"OrderUuid"           => $orderUuid
		);
		
		/* Invoke webservice method with your parameters */
		$soapClient->__setSoapHeaders($this->getHeader());
		$response = null;
		
		try {
			$response = $soapClient->monitorOrder($params);
		} catch (\SoapFault $soapFault) {
			$this->exitWithError($soapFault);
		}
		
		$status = $response->Order->Status;
		
		if ($status->Status->code === self::ORDER_STATUS_SUCCESS) {
			if (isset($response->CustomerRelationUuid)) {
				/** @var Pairing $pairing */
				$pairing = $this->pairingRepository->findOneByTwintOrderUuid($orderUuid);
				if ($pairing instanceof Pairing) {
					$user = $pairing->getUser();
					$this->updateCustomerRelation($user, $response->CustomerRelationUuid);
				}
			}
		}
		
		return $response;
	}
	
	/**
	 * Cancel a specific order with a given twint order uuid
	 *
	 * @param $orderUuid
	 * @return mixed
	 */
	public function cancelOrder($orderUuid) {
		// get soap client
		$soapClient = $this->getSoapClient();
		
		// Prepare the parameters for the request
		$params = array(
			"MerchantInformation" => $this->merchantInformation,
			"OrderUuid"           => $orderUuid
		);
		
		// Invoke webservice method with your parameters
		$soapClient->__setSoapHeaders($this->getHeader());
		$response = null;
		
		try {
			$response = $soapClient->cancelOrder($params);
		} catch (\SoapFault $soapFault) {
			$this->exitWithError($soapFault);
		}
		
		return $response;
	}
	
	/**
	 * Returns a message header
	 *
	 * @return array
	 */
	private function getHeader() {
		$headers = array();
		$request_header = array(
			"MessageId"             => $this->getNewMessageId(),
			"ClientSoftwareName"    => $this->settings['software']['name'],
			"ClientSoftwareVersion" => $this->settings['software']['version']
		);
		$headers[] = new \SOAPHeader('http://service.twint.ch/header/types/v2', 'RequestHeaderElement', $request_header);
		
		return $headers;
	}
	
	/**
	 * Adds either a twint customerRelationUuid or an unidentifiedCustomer indicator to the parameters array
	 *
	 * @param array $parameter
	 * @param User  $user
	 */
	private function addCustomerIdentification(array &$parameter, User $user) {
		/** @var TwintCustomerRelation $twintCustomerRelation */
		$twintCustomerRelation = $this->twintCustomerRelationRepository->findOneByCustomer($user);
		
		if (!is_null($twintCustomerRelation)) {
			$parameter['CustomerRelationUuid'] = $twintCustomerRelation->getTwintCustomerRelation();
		} else {
			$parameter['UnidentifiedCustomer'] = true;
		}
	}
	
	/**
	 * Exit program execution with error
	 *
	 * @TODO: Implement nice error handling
	 *
	 * @param \SoapFault $soapFault
	 */
	private function exitWithError(\SoapFault $soapFault) {
		$msg = sprintf("ERROR: The TWINT Server returned Error-Code '%s' with the following message: %s ()", $soapFault->faultcode, $soapFault->faultstring);
		exit($msg);
	}
	
	/**
	 * Returns a soap client
	 *
	 * @return \SoapClient
	 */
	private function getSoapClient() {
		if (is_null($this->soapClient)) {
			$wsdlPath = $this->resourceManager->getPublicPackageResourceUriByPath($this->settings['wsdl']['merchant']);
			$this->soapClient = new \SoapClient($wsdlPath, $this->getSoapOptions());
			$this->merchantInformation = array(
				"MerchantUuid"   => $this->settings['merchantUuid'],
				"CashRegisterId" => $this->settings['cashRegisterId']
			);
		}
		
		return $this->soapClient;
	}
	
	/**
	 * Persist a user pairing
	 *
	 * @param User   $user
	 * @param string $pairingUuid
	 */
	private function persistPairing(User $user, $pairingUuid) {
		$pairing = new Pairing();
		$pairing->setUser($user);
		$pairing->setPairingUuid($pairingUuid);
		$this->pairingRepository->add($pairing);
		$this->persistenceManager->persistAll();
	}
	
	/**
	 * Create or update customer relation with twint customer relation uuid
	 *
	 * @param User   $user
	 * @param string $customerRelationUuid
	 */
	private function updateCustomerRelation(User $user, $customerRelationUuid) {
		/** @var TwintCustomerRelation $customerRelation */
		$customerRelation = $this->twintCustomerRelationRepository->findOneByCustomer($user);
		
		$doPersist = false;
		if ($customerRelation instanceof TwintCustomerRelation) {
			// customer relation exists --> update customer relation uuid of not equal with existing uuid
			if ($customerRelation->getTwintCustomerRelation() !== $customerRelationUuid) {
				$customerRelation->setTwintCustomerRelation($customerRelationUuid);
				$this->twintCustomerRelationRepository->update($customerRelation);
				$doPersist = true;
			}
		} else {
			// no customer relation exists yet --> create new customer relation
			$newCustomerRelation = new TwintCustomerRelation();
			$newCustomerRelation->setCustomer($user);
			$newCustomerRelation->setTwintCustomerRelation($customerRelationUuid);
			$this->twintCustomerRelationRepository->add($newCustomerRelation);
			$doPersist = true;
		}
		
		if ($doPersist) {
			$this->persistenceManager->persistAll();
		}
	}
	
	/**
	 * Returns a uuid in V4 format as message id
	 *
	 * @return string
	 */
	private function getNewMessageId() {
		$data = openssl_random_pseudo_bytes(16);
		
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
		
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}
	
	/**
	 * Returns the predefined soap options
	 *
	 * @return array
	 */
	private function getSoapOptions() {
		$options = $this->settings['soapOptions'];
		$options['local_cert'] = FLOW_PATH_ROOT . $options['local_cert'];
		
		return $options;
	}
}