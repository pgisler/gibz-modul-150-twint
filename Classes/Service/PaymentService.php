<?php
namespace One50\Twint\Service;

use One50\Shop\Domain\Model\Order;
use One50\Shop\Domain\Model\User;
use One50\Shop\Service\PaymentProviderInterface;
use One50\Twint\Domain\Model\OrderMapping;
use One50\Twint\Domain\Model\TwintCustomerRelation;
use One50\Twint\Domain\Repository\OrderMappingRepository;
use One50\Twint\Domain\Repository\TwintCustomerRelationRepository;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\PersistenceManagerInterface;

class PaymentService implements PaymentProviderInterface {
	
	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 */
	protected $persistenceManager;
	
	/**
	 * @var TwintCustomerRelationRepository
	 * @Flow\Inject
	 */
	protected $customerRelationRepository;
	
	/**
	 * @var OrderMappingRepository
	 * @Flow\Inject
	 */
	protected $orderMappingRepository;
	
	/**
	 * @var SoapService
	 * @Flow\Inject
	 */
	protected $twintService;
	
	/**
	 * @param Order $order
	 * @return mixed
	 */
	public function startPayment(Order $order) {
		$twintCustomerUuid = $this->getTwintCustomerUuid($order->getUser());
		$totalOrderSum = $order->getTotalSum();
		
		/**
		 * Debugging mode: prevent recuring customer recognition
		 * @TODO: remove static "null" value
		 */
		$twintCustomerUuid = null;
		
		// enroll cash register
		$this->twintService->enrollCashRegister();
		
		if (is_null($twintCustomerUuid)) {
			// initiate pairing since no customer relation exists
			$response = $this->twintService->requestCheckIn($order->getUser(), true);
		} else {
			// use existing customer identification
			$response = $this->twintService->startOrder($totalOrderSum, $order->getTransactionId(), null, $twintCustomerUuid);
			$twintOrderUuid = $response->OrderUuid;
			$this->createOrderMapping($order, $twintOrderUuid);
		}
		
		return $response;
	}
	
	/**
	 * Return the pairing status for a specific pairing
	 *
	 * @param $pairingUuid
	 * @return mixed
	 */
	public function checkPairing($pairingUuid) {
		$response = $this->twintService->monitorCheckIn($pairingUuid);
		if (isset($response->CheckInNotification) && isset($response->CheckInNotification->PairingStatus)) {
			$pairingStatus = $response->CheckInNotification->PairingStatus;
		} else {
			$pairingStatus = null;
		}
		
		return $pairingStatus;
	}
	
	/**
	 * Checks the payment status of a given order and returns the payment status code
	 *
	 * @param Order $order
	 * @return int|null
	 */
	public function checkPaymentStatus(Order $order) {
		$twintOrderUuid = $this->getTwintOrderUuid($order);
		if (!is_null($twintOrderUuid)) {
			$twintOrder = $this->twintService->monitorOrderStatus($twintOrderUuid);
			$twintOrderStatusCode = $twintOrder->Order->Status->Status->code;
			$orderStatusCode = $this->getOrderStatusCode($twintOrderStatusCode);
		} else {
			$orderStatusCode = PaymentProviderInterface::PAYMENT_STATUS_UNKNOWN;
		}
		
		return $orderStatusCode;
	}
	
	/**
	 * Start an order with either pairing uuid or customer relation uuid
	 *
	 * @param Order  $order
	 * @param string $pairingUuid
	 * @param string $customerRelationUuid
	 * @return mixed
	 */
	public function startOrder(Order $order, $pairingUuid = null, $customerRelationUuid = null) {
		$amount = $order->getTotalSum();
		$transactionReference = $this->persistenceManager->getIdentifierByObject($order);
		$response = $this->twintService->startOrder($amount, $transactionReference, $pairingUuid, $customerRelationUuid);
		return $response;
	}
	
	/**
	 * Returns the twint customer relation uuid for a shop user.
	 * Null will be returned if no twint customer relation uuid exists for the given user.
	 *
	 * @param User $user
	 * @return null|string
	 */
	private function getTwintCustomerUuid(User $user) {
		$customerRelation = $this->customerRelationRepository->findOneByCustomer($user);
		
		return ($customerRelation instanceof TwintCustomerRelation) ? $customerRelation->getTwintCustomerRelation() : null;
	}
	
	/**
	 * Create and persist a new order mapping
	 *
	 * @param Order  $order
	 * @param string $twintOrderUuid
	 */
	private function createOrderMapping(Order $order, $twintOrderUuid) {
		$orderMapping = new OrderMapping();
		$orderMapping->setShopOrder($order);
		$orderMapping->setTwintOrderUuid($twintOrderUuid);
		$this->orderMappingRepository->add($orderMapping);
		$this->persistenceManager->persistAll();
	}
	
	/**
	 * Returns a twint order uuid for a given order.
	 * Null will be returned if no twint order uuid is available for the given order.
	 *
	 * @param Order $order
	 * @return string|null
	 */
	private function getTwintOrderUuid(Order $order) {
		$orderMapping = $this->orderMappingRepository->findOneByShopOrder($order);
		$twintOrderUuid = null;
		if ($orderMapping instanceof OrderMapping) {
			$twintOrderUuid = $orderMapping->getTwintOrderUuid();
		}
		
		return $twintOrderUuid;
	}
	
	/**
	 * Transforms the given twint order status code into an official order status code according to the
	 * PaymentProviderInterface
	 *
	 * @param $twintOrderStatusCode
	 * @return integer|null
	 */
	private function getOrderStatusCode($twintOrderStatusCode) {
		$statusCode = null;
		switch ($twintOrderStatusCode) {
			case SoapService::ORDER_STATUS_SUCCESS:
				$statusCode = PaymentProviderInterface::PAYMENT_STATUS_SUCCESS;
				break;
			case SoapService::ORDER_STATUS_IN_PROGRESS:
				$statusCode = PaymentProviderInterface::PAYMENT_STATUS_IN_PROGRESS;
				break;
			case SoapService::ORDER_STATUS_FAILURE:
				$statusCode = PaymentProviderInterface::PAYMENT_STATUS_FAILURE;
				break;
			default:
				$statusCode = PaymentProviderInterface::PAYMENT_STATUS_UNKNOWN;
				break;
		}
		
		return $statusCode;
	}
	
	
}