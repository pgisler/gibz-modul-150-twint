<?xml version="1.0" encoding="UTF-8"?>
<xs:schema
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:base="http://service.twint.ch/base/types/v2"
	xmlns:tns="http://service.twint.ch/voucher/types/v2"
	targetNamespace="http://service.twint.ch/voucher/types/v2"
	elementFormDefault="qualified" version="1.0">

	<xs:import schemaLocation="twint_base_types_v2_0.xsd" namespace="http://service.twint.ch/base/types/v2" />

	<xs:element name="CreateVoucherRequestElement" type="tns:CreateVoucherRequestType" />
	<xs:element name="CreateVoucherResponseElement" type="tns:VoucherInformationType" />
	<xs:element name="ActivateVoucherRequestElement" type="tns:VoucherIdentifierType" />
	<xs:element name="ActivateVoucherResponseElement" type="tns:VoucherBaseInformationType" />
	<xs:element name="CancelVoucherRequestElement" type="tns:VoucherIdentifierType" />
	<xs:element name="CancelVoucherResponseElement" type="tns:CancelVoucherResponseType" />
	<xs:element name="GetVoucherRequestElement" type="tns:VoucherIdentifierType" />
	<xs:element name="GetVoucherResponseElement" type="tns:VoucherBaseInformationType" />
	<xs:element name="CheckSystemStatusRequestElement" type="tns:CheckSystemStatusRequestType" />
	<xs:element name="CheckSystemStatusResponseElement" type="tns:CheckSystemStatusResponseType" />

	<!-- Interface Types -->
	<xs:complexType name="CreateVoucherRequestType">
		<xs:sequence>
			<xs:element name="MerchantInformation" type="base:MerchantInformationBaseType" />
			<xs:element name="Value" type="base:CurrencyAmountType" />
			<xs:element name="CategoryCode" type="xs:integer" />
			<xs:element name="MerchantTransactionReference" type="base:MerchantTransactionReferenceType" />
		</xs:sequence>
		<xs:attribute name="AutoActivate" type="xs:boolean" default="true" />
		<xs:attribute name="SerialNumberBarcodeStyle" type="tns:VoucherBarcodeStyle" default="NONE" />
		<xs:attribute name="PinBarcodeStyle" type="tns:VoucherBarcodeStyle" default="NONE" />
	</xs:complexType>

	<xs:complexType name="VoucherBaseInformationType">
		<xs:annotation>
			<xs:documentation>
				Contains the basic information about a Voucher that can be passed around in	all Responses.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Value" type="base:CurrencyAmountType" />
			<xs:element name="State" type="tns:VoucherState" />
			<xs:element name="SerialNumber" type="base:Token50Type" />
			<xs:element name="MerchantTransactionReference" type="base:MerchantTransactionReferenceType" />
			<xs:element name="ValidUntil" type="xs:date" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="VoucherInformationType">
		<xs:annotation>
			<xs:documentation>
				Contains specific information about a voucher (including all Serial-Numbers
				and the PIN) that should only be returned as answer to the initial (createVoucher) Call.
			</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="tns:VoucherBaseInformationType">
				<xs:sequence>
					<xs:element name="Pin" type="base:Token50Type" />
					<xs:element name="CategoryCode" type="xs:integer" />
					<xs:element name="SerialNumberBarcode" type="base:DataUriScheme" minOccurs="0">
						<xs:annotation>
							<xs:documentation>
								The serial number rendered as image. Only provided if requested in the query.
							</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="PinBarcode" type="base:DataUriScheme" minOccurs="0">
						<xs:annotation>
							<xs:documentation>
								The PIN number rendered as image. Only provided if requested in the query.
							</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<xs:complexType name="VoucherIdentifierType">
		<xs:annotation>
			<xs:documentation>
				Uniquely identifies a voucher.
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="MerchantInformation" type="base:MerchantInformationBaseType" />
			<xs:choice>
				<xs:element name="SerialNumber" type="base:Token50Type" />
				<xs:element name="MerchantTransactionReference" type="base:MerchantTransactionReferenceType" />
			</xs:choice>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="CancelVoucherResponseType">
		<xs:sequence>
			<xs:element name="SerialNumber" type="base:Token50Type" />
			<xs:element name="State" type="tns:VoucherState" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="CheckSystemStatusRequestType">
		<xs:sequence>
			<xs:element name="MerchantInformation" type="base:MerchantInformationBaseType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="CheckSystemStatusResponseType">
		<xs:sequence>
			<xs:element name="Status" type="tns:OperationResultType" />
		</xs:sequence>
	</xs:complexType>


	<!-- Common types -->
	<xs:simpleType name="VoucherBarcodeStyle">
		<xs:restriction base="xs:string">
			<xs:enumeration value="NONE">
				<xs:annotation>
					<xs:documentation>
						None.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="QRCODE">
				<xs:annotation>
					<xs:documentation>
						QR Code (Quick Response Code).
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="VoucherState">
		<xs:restriction base="xs:string">
			<xs:enumeration value="NEW">
				<xs:annotation>
					<xs:documentation>
						Created, but not activated yet (ERSTELLT). The
						voucher  is worthless at the moment.
						Can transition to ACTIVE or CANCELED.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="ACTIVE">
				<xs:annotation>
					<xs:documentation>
						Activated (GUELTIG). The voucher can be used now.
						Can transition to CASHED_IN, CANCELED and EXPIRED.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="CASHED_IN">
				<xs:annotation>
					<xs:documentation>
						Cashed-in by the user (EINGELOEST). Can not
						transition to any other state.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="CANCELED">
				<xs:annotation>
					<xs:documentation>
						Canceled (ZURUECKGERUFEN). Can not transition to
						any other state.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
			<xs:enumeration value="EXPIRED">
				<xs:annotation>
					<xs:documentation>
						Timed out (ABGELAUFEN). Can transition to ACTIVE.
					</xs:documentation>
				</xs:annotation>
			</xs:enumeration>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="OperationResultType">
		<xs:restriction base="xs:string">
			<xs:enumeration value="OK" />
			<xs:enumeration value="ERROR" />
		</xs:restriction>
	</xs:simpleType>

</xs:schema>